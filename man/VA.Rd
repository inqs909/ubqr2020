\name{VA}
\alias{VA}
\docType{data}
\title{
Veteran's Administration Lung Cancer Study
}
\description{
This is the VA dataset. A survival analysis method will be used.
}
\usage{data("VA")}
\format{
  A data frame with 137 observations on the following 7 variables.
  \describe{
    \item{\code{trt}}{1=standard,2=experimental}
    \item{\code{celltype}}{Type of cell}
    \item{\code{time}}{survival time (outcome)}
    \item{\code{status}}{1=Dead,0=Alive (outcome)}
    \item{\code{karno}}{Karnofsky score, 0=bad,100=good}
    \item{\code{age}}{in years}
    \item{\code{prior}}{prior therapy, 0=no,10=yes}
  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(VA)
## maybe str(VA)
}
\keyword{datasets}
