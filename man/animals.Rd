\name{animals}
\alias{animals}
\docType{data}
\title{
Animals Dataset
}
\description{
This is the animals dataset. A Poisson regression is used to analyse.
}
\usage{data("animals")}
\format{
  A data frame with 70 observations on the following 7 variables.
  \describe{
    \item{\code{MSW93_Genus}}{The genus of a species.}
    \item{\code{MSW93_Species}}{The species.}
    \item{\code{AdultBodyMass}}{Body Mass}
    \item{\code{HabitatBreadth}}{Habitat Layer}
    \item{\code{TrophicLevel}}{Diet}
    \item{\code{SocialGroup}}{Group Composity and Size}
    \item{\code{PopDensity}}{Population Density (Outcome)}
  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(animals)
## maybe str(animals)
}
\keyword{datasets}
