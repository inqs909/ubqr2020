\name{student}
\alias{student}
\docType{data}
\title{
Student Mathematical Grades
}
\description{
This is the students math grades dataset. Use a linear regression approach.
}
\usage{data("student")}
\format{
  A data frame with 395 observations on the following 8 variables.
  \describe{
    \item{\code{age}}{in years}
    \item{\code{sex}}{F-Female, M-Male}
    \item{\code{internet}}{access to internet, yes or no}
    \item{\code{higher}}{aspire for higher education, yes or no}
    \item{\code{health}}{health status, 1-very bad, 5-very good}
    \item{\code{famsup}}{educational support from family, yes or no}
    \item{\code{paid}}{paid educational support, yes or no}
    \item{\code{G3}}{Final Math Grades, 0 to 20 (outcome)}
  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(student)
## maybe str(student) ; plot(student) ...
}
\keyword{datasets}
