# Upward Bound 2020 Public Health R Package

## General Information
This repository contains information on UBQR2020 R Package. 
This R package contains materials used for my Upward Bound Public Health course.
It contains learnr tutorials for my students to complete and submit.
It contains data for my students to conduct data analysis projects.

## learnr Tutorials

- Introduction to R (Intro_R)
- Biostatistics (Biostats)
- SIR Models (Sir_Models)
- Environmental Health (Environmental_Health)
- Health Disparities (Health_Disparities)

## Install R Package

To install the package, use the following code:
```
#install.packages("devtools")
#Intsall R Package
devtools::install_gitlab("inqs909/UBQR2020")
```
Make sure to install the devtools package first.

## Access learnr tutorial

To access one of my learnr tutorials, use the `access_tutorial()` function. It only needs the name of the tutorial.

```
library(UBQR2020)
access_tutorial("Intro_R")
```

## Access CSV File

My learnr tutorials record a student's activity and saves it in a CSV file. Students can turn in this CSV file as there submission. To retrieve the file, use the `getting_work()` function. It will obtain the file and save your file in the working directory and named as "YOUR_NAME-INTRO_R-Submission.csv". This function needs the name of the tutorial and your name to work.

```
getting_work("Intro_R","YOUR_NAME")
```

## R Package Information

This package is designed to provide my Upward Bound students with a tutorial on different topics and get an idea about programming in R. Therefore, there are 3 built-in functions designed to help students access the tutorial, record their answers and have obtain a csv file to submit for grading. The functions are the `access_tutorial()`, `getting_work()`, and `writing_tutorial()` functions.

### access_tutorial()

To access one of my learnr tutorials, use the `access_tutorial()` function. It only needs the name of the tutorial.

```
library(UBQR2020)
access_tutorial("Intro_R")
```

### getting_work()

My learnr tutorials record a student's activity and saves it in a CSV file. Students can turn in this CSV file as there submission. To retrieve the file, use the `getting_work()` function. It will obtain the file and save your file in the working directory and named as "YOUR_NAME-INTRO_R-Submission.csv". This function needs the name of the tutorial and your name to work.

```
getting_work("Intro_R","YOUR_NAME")
```

### writing_tutorial()

The `writing_tutorial()` function is used to record a students progress. As the student progresses from the learnr tutorial, it will record an action is committed. The action is then saved in a CSV file that a student can later obtain using the `getting_work()` function. The `writiing_tutorial()` is placed at the beginning of an Rmd file to record a students' work.

